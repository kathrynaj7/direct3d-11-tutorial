#include "Structures.h"

using namespace DirectX;

XMFLOAT3 operator*(float f, XMFLOAT3 xf)
{
	return XMFLOAT3(f*xf.x, f*xf.y, f*xf.z);
}

XMFLOAT3 operator*(XMFLOAT3 xf, float f)
{
	return XMFLOAT3(f*xf.x, f*xf.y, f*xf.z);
}

XMFLOAT3 operator+(XMFLOAT3 xf1, XMFLOAT3 xf2)
{
	return XMFLOAT3(xf1.x + xf2.x, xf1.y + xf2.y, xf1.z + xf2.z);
}