#ifndef SHADER_H
#define SHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <string>

class Graphics;

class Shader
{
public:
	virtual HRESULT InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext) = 0;
	virtual void SetShader(ID3D11DeviceContext* g_pImmediateContext) = 0;
	virtual void CleanUp() = 0;

protected:
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
};

#endif