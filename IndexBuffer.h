#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <vector>
#include "Structures.h"

class IndexBuffer
{
private:
	ID3D11Buffer* g_pIndexBuffer = NULL;

public:
	HRESULT CreateIndexBuffer(ID3D11Device* g_pd3dDevice, std::vector<unsigned int> indices);
	void SetActiveBuffer(ID3D11DeviceContext* g_pImmediateContext);
	void Release();
};

#endif