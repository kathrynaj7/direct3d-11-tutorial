#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "resource.h"

class Window;
class ConstantBuffer;

class Graphics
{
private:
	D3D_DRIVER_TYPE				g_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL			g_featureLevel = D3D_FEATURE_LEVEL_11_0;
	ID3D11Device*				g_pd3dDevice = NULL;
	ID3D11DeviceContext*		g_pImmediateContext = NULL;
	IDXGISwapChain*				g_pSwapChain = NULL;
	ID3D11RenderTargetView*		g_pRenderTargetView = NULL;
	ID3D11Texture2D*			g_pDepthStencil = NULL;
	ID3D11DepthStencilState *	g_pDSState = NULL;
	ID3D11DepthStencilView*		g_pDepthStencilView = NULL;
	D3D11_RASTERIZER_DESC		g_prsDesc;
	ID3D11RasterizerState*		g_pRasterizerState = NULL;
	ID3D11Buffer*				g_pConstantBuffer = NULL;
	ID3D11SamplerState*			g_pSamplerLinear = NULL;
	Window*						window;
	ConstantBuffer*				projectionConstBuffer;
	ID3D11Buffer*				worldBuffer;

public:
	Graphics(Window * window);
	~Graphics(void);

	HRESULT InitDevice(HWND g_hWnd);
	HRESULT CreateDeviceAndSwapCahin(UINT width, UINT height, HWND g_hWnd);
	HRESULT CreateRenderTargetView();
	HRESULT CreateDepthStencilTexture(UINT width, UINT height);
	HRESULT CreateDepthStencilState();
	HRESULT CreateDepthStencilView();
	void SetViewport(UINT width, UINT height);
	HRESULT CreateRasterizerState();
	HRESULT UpdateRasterizer(D3D11_RASTERIZER_DESC desc);
	HRESULT CreateLinearSampler();
	void CleanupDevice();
	void ClearBackBuffer();
	void ClearDepthStencil();
	void PresentSwapChain();
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetImmediateContext();
	ID3D11Buffer* GetWorldBuffer();
	Window* GetWindow();
	D3D11_RASTERIZER_DESC GetRasterizerDesc();
	ID3D11RasterizerState* GetRasterizerState();
};

#endif