#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <vector>
#include "Structures.h"

class ModelLoader
{
public:
	static bool LoadModel(std::string name, std::vector<Vertex>& vertices, std::vector<unsigned int>& indices);

	static DirectX::XMFLOAT2 ProcessLineToFloat2(std::string line);
	static DirectX::XMFLOAT3 ProcessLineToFloat3(std::string line);
	static void ProcessFace(std::vector<Vertex>& vertices, 
							std::string line, 
							std::vector<DirectX::XMFLOAT3>fileVertices, 
							std::vector<DirectX::XMFLOAT3>fileNormals, 
							std::vector<DirectX::XMFLOAT2>fileTextures, 
							int &vertexCount);
	static void CalculateTangents(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices);

	static float DotProduct(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two);
	static DirectX::XMFLOAT3 XMMultiplyByFloat(DirectX::XMFLOAT3 one, float two);
	static DirectX::XMFLOAT3 XMSubtract(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two);
	static DirectX::XMFLOAT3 XMNormalise(DirectX::XMFLOAT3 one);
};


#endif