#ifndef PIXELSHADER_H
#define PIXELSHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Shader.h"

class Graphics;

class PixelShader : Shader
{
public:
	virtual HRESULT InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext);
	virtual void SetShader(ID3D11DeviceContext* g_pImmediateContext);
	virtual void CleanUp();

private:
	ID3DBlob*			pPSBlob = NULL;
	ID3D11PixelShader*	g_pPixelShader = NULL;
};

#endif