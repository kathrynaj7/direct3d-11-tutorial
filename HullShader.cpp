#include "HullShader.h"
#include "Graphics.h"

HRESULT HullShader::InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext)
{
	HRESULT hr = CompileShaderFromFile(name.c_str(), "HS", "hs_5_0", &pHSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = g_pd3dDevice->CreateHullShader(pHSBlob->GetBufferPointer(), pHSBlob->GetBufferSize(), NULL, &g_pHullShader);
	pHSBlob->Release();
	if (FAILED(hr))
		return hr;
}

void HullShader::SetShader(ID3D11DeviceContext* g_pImmediateContext)
{
	g_pImmediateContext->HSSetShader(g_pHullShader, NULL, 0);
}

void HullShader::CleanUp()
{
	g_pHullShader->Release();
}