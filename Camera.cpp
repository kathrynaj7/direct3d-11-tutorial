#include "Camera.h"
#include "Graphics.h"
#include "ConstantBuffer.h"

using namespace DirectX;

Camera::Camera(Graphics* graphics)
{
	this->graphics = graphics;
}

Camera::~Camera(void)
{
}

HRESULT Camera::InitCamera(XMFLOAT3 position, XMFLOAT3 direction, XMFLOAT3 up, XMFLOAT3 right)
{
	ConstantBuffer* viewConstBuffer = new ConstantBuffer();
	viewConstBuffer->CreateConstantBuffer<ViewMatrix>(graphics->GetDevice());
	viewBuffer = viewConstBuffer->GetBuffer();

	ConstantBuffer* cameraDataConstBuffer = new ConstantBuffer();
	cameraDataConstBuffer->CreateConstantBuffer<CameraData>(graphics->GetDevice());
	cameraDataBuffer = cameraDataConstBuffer->GetBuffer();

	this->position = position;
	this->direction = direction;
	this->up = up;
	this->right = right;

	return S_OK;
}

void Camera::Update()
{
	XMVECTOR R = XMLoadFloat3(&right);
	XMVECTOR U = XMLoadFloat3(&up);
	XMVECTOR L = XMLoadFloat3(&direction);
	XMVECTOR P = XMLoadFloat3(&position);

	L = XMVector3Normalize(L);
	U = XMVector3Normalize(XMVector3Cross(L, R));

	R = XMVector3Cross(U, L);

	// Fill in the view matrix entries.
	float x = -XMVectorGetX(XMVector3Dot(P, R));
	float y = -XMVectorGetX(XMVector3Dot(P, U));
	float z = -XMVectorGetX(XMVector3Dot(P, L));

	XMStoreFloat3(&right, R);
	XMStoreFloat3(&up, U);
	XMStoreFloat3(&direction, L);

	XMFLOAT4X4 viewMatrix = XMFLOAT4X4(right.x, up.x, direction.x, 0.0f,
									   right.y, up.y, direction.y, 0.0f,
									   right.z, up.z, direction.z, 0.0f,
									   x, y, z, 1.0f);

	ViewMatrix matrix;
	matrix.View = XMLoadFloat4x4(&viewMatrix);

	graphics->GetImmediateContext()->UpdateSubresource(viewBuffer, 0, NULL, &viewMatrix, 0, 0);
	graphics->GetImmediateContext()->VSSetConstantBuffers(1, 1, &viewBuffer);
	
	CameraData camData;
	camData.CameraPos = position;

	graphics->GetImmediateContext()->UpdateSubresource(cameraDataBuffer, 0, NULL, &camData, 0, 0);
	graphics->GetImmediateContext()->PSSetConstantBuffers(3, 1, &cameraDataBuffer);
}

void Camera::Cleanup()
{
	if (viewBuffer) viewBuffer->Release();
	if (cameraDataBuffer) cameraDataBuffer->Release();
}

void Camera::Walk(float deltaTime)
{
	XMVECTOR time = XMVectorReplicate(deltaTime);
	XMVECTOR look = XMLoadFloat3(&direction);
	XMVECTOR location = XMLoadFloat3(&position);
	XMStoreFloat3(&position, XMVectorMultiplyAdd(time, look, location));
}

void Camera::Strafe(float deltaTime)
{
	XMVECTOR time = XMVectorReplicate(deltaTime);
	XMVECTOR turn = XMLoadFloat3(&right);
	XMVECTOR location = XMLoadFloat3(&position);
	XMStoreFloat3(&position, XMVectorMultiplyAdd(time, turn, location));
}

void Camera::Pitch(float angle)
{
	XMMATRIX rotation = XMMatrixRotationAxis(XMLoadFloat3(&right), angle);

	XMStoreFloat3(&up, XMVector3TransformNormal(XMLoadFloat3(&up), rotation));
	XMStoreFloat3(&direction, XMVector3TransformNormal(XMLoadFloat3(&direction), rotation));
}

void Camera::RotateY(float angle)
{
	XMMATRIX rotation = XMMatrixRotationY(angle);

	XMStoreFloat3(&right, XMVector3TransformNormal(XMLoadFloat3(&right), rotation));
	XMStoreFloat3(&up, XMVector3TransformNormal(XMLoadFloat3(&up), rotation));
	XMStoreFloat3(&direction, XMVector3TransformNormal(XMLoadFloat3(&direction), rotation));
}

ViewMatrix Camera::getView()
{
	return view;
}
