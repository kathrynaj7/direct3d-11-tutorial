#ifndef GAME_H
#define GAME_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <vector>
#include "Structures.h"

class Graphics;
class Object;
class Camera;

class Game
{
private:
	Graphics* graphics;
	Camera* camera;
	std::vector<Object*> objects;
	std::vector<BezierSpline*> splines;

	float lastMousePosX = NULL;
	float lastMousePosY = NULL;

public:
	Game(Graphics * graphics);
	~Game(void);

	HRESULT InitGame();
	MSG Run(HWND g_hWnd);
	void CleanUp();
};

#endif