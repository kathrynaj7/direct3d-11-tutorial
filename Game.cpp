#include <directxmath.h>
#include "Game.h"
#include "Graphics.h"
#include "Window.h"
#include "Object.h"
#include "Camera.h"

using namespace DirectX;

Game::Game(Graphics* graphics)
{
	this->graphics = graphics;
}

Game::~Game(void)
{
}

HRESULT Game::InitGame()
{
	Object* cube1 = new Object(graphics);
	cube1->InitObject("cube2.obj", 
					  L"Crate_COLOR.dds", 
					  L"Crate_SPEC.dds",
					  L"Crate_NRM.dds",
					  L"Bump.fx", 
					  L"Bump.fx", 
					  L"", L"",
					  XMFLOAT3(-1.0f, 1.0f, 3.0f), 
					  XMFLOAT3(0.0f, 0.5f, 0.5f), 
					  XMFLOAT3(1.0f, 1.0f, 1.0f));
	objects.push_back(cube1);

	Object* cube2 = new Object(graphics);
	cube2->InitObject("sphere.obj", 
					  L"clouds2Env.dds",
					  L"whiteTex.dds",
					  L"blankNormal.dds",
					  L"Environment.fx", 
					  L"Environment.fx",
					  L"", L"",
					  XMFLOAT3(0.5f, 1.5f, 3.0f), 
					  XMFLOAT3(0.0f, 0.0f, 0.0f), 
					  XMFLOAT3(0.25f, 0.25f, 0.25f));
	objects.push_back(cube2);

	Object* room = new Object(graphics);
	room->InitObject("sphereRoom.obj", 
					 L"cloudsEnv.dds", 
					 L"whiteTex.dds",
					 L"blankNormal.dds",
					 L"Skybox.fx", 
					 L"Skybox.fx",
					 L"", L"",
					 XMFLOAT3(0.0f, 0.0f, 0.0f), 
					 XMFLOAT3(0.0f, 0.0f, 0.0f), 
					 XMFLOAT3(1.0f, 1.0f, 1.0f));
	objects.push_back(room);

	Object* floor = new Object(graphics);
	floor->InitObject("floor.obj",
					  L"whiteTex.dds",
					  L"whiteTex.dds",
					  L"Floor.dds",
					  L"Bump.fx",
					  L"Bump.fx",
					  L"", L"",
					  XMFLOAT3(0.0f, -1.0f, 0.0f),
					  XMFLOAT3(0.0f, 0.0f, 0.0f),
					  XMFLOAT3(0.5f, 0.5f, 0.5f));
	objects.push_back(floor);

	Object* sphere2 = new Object(graphics);
	sphere2->InitObject("sphere.obj",
						L"BrickTexture.dds",
						L"whiteTex.dds",
						L"blankNormal.dds",
						L"Tesselate.fx",
						L"Tesselate.fx",
						L"Tesselate.fx",
						L"Tesselate.fx",
						XMFLOAT3(2.0f, 1.0f, 3.0f),
						XMFLOAT3(0.0f, 0.0f, 0.0f),
						XMFLOAT3(0.2f, 0.2f, 0.2f));
	objects.push_back(sphere2);

	Object* sphere3 = new Object(graphics);
	sphere3->InitObject("sphere.obj",
						L"cloudsEnv.dds",
						L"whiteTex.dds",
						L"blankNormal.dds",
						L"Environment.fx",
						L"Environment.fx",
						L"", L"",
						XMFLOAT3(0.0f, 0.0f, -5.0f),
						XMFLOAT3(0.0f, 0.0f, 0.0f),
						XMFLOAT3(0.2f, 0.2f, 0.2f));
	objects.push_back(sphere3);

	camera = new Camera(graphics);
	camera->InitCamera(XMFLOAT3(0.0f, 2.0f, -5.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f));

	BezierSpline* spline1 = new BezierSpline(XMFLOAT3(-5.0f, 0.0f, 0.0f),
											 XMFLOAT3(-5.0f, 0.0f, 5.0f),
											 XMFLOAT3(-2.5f, 0.0f, 10.0f),
											 XMFLOAT3(0.0f, 0.0f, 10.0f));
	splines.push_back(spline1);
	BezierSpline* spline2 = new BezierSpline(XMFLOAT3(0.0f, 0.0f, 10.0f),
											 XMFLOAT3(2.5f, 0.0f, 10.0f),
											 XMFLOAT3(5.0f, 0.0f, 5.0f),
											 XMFLOAT3(5.0f, 0.0f, 0.0f));
	splines.push_back(spline2);
	BezierSpline* spline3 = new BezierSpline(XMFLOAT3(5.0f, 0.0f, 0.0f),
											 XMFLOAT3(5.0f, 0.0f, -5.0f),
											 XMFLOAT3(2.5f, 0.0f, -10.0f),
											 XMFLOAT3(0.0f, 0.0f, -10.0f));
	splines.push_back(spline3);
	BezierSpline* spline4 = new BezierSpline(XMFLOAT3(0.0f, 0.0f, -10.0f),
											 XMFLOAT3(-2.5f, 0.0f, -10.0f),
											 XMFLOAT3(-5.0f, 0.0f, -5.0f),
											 XMFLOAT3(-5.0f, 0.0f, 0.0f));
	splines.push_back(spline4);

	return S_OK;
}

MSG Game::Run(HWND g_hWnd)
{
	MSG msg = { 0 };
	float previousFrame = 0.0f;
	float progress = 0.0f;
	unsigned int splineIndex = 0;

	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			unsigned int start = timeGetTime();

			// Clear the back buffer and depth stencil
			graphics->ClearBackBuffer();
			graphics->ClearDepthStencil();

			//input.Update(graphics->getWindow()->getMousePosX(), graphics->getWindow()->getMousePosY(), previousFrame);

			if (GetAsyncKeyState(VK_ESCAPE) & 0x8000)
				PostMessage(g_hWnd, WM_DESTROY, 0, 0);

			if (GetAsyncKeyState('Z') & 1)
			{
				D3D11_RASTERIZER_DESC desc = graphics->GetRasterizerDesc();
				if (desc.FillMode == D3D11_FILL_SOLID)
					desc.FillMode = D3D11_FILL_WIREFRAME;
				else
					desc.FillMode = D3D11_FILL_SOLID;
				graphics->UpdateRasterizer(desc);
			}

			if (GetAsyncKeyState('W') & 0x8000)
				camera->Walk(previousFrame*2.0f);
			if (GetAsyncKeyState('S') & 0x8000)
				camera->Walk(-previousFrame*2.0f);
			if (GetAsyncKeyState('A') & 0x8000)
				camera->Strafe(-previousFrame*2.0f);
			if (GetAsyncKeyState('D') & 0x8000)
				camera->Strafe(previousFrame*2.0f);

			float mouseX = graphics->GetWindow()->getMousePosX();
			float mouseY = graphics->GetWindow()->getMousePosY();

			if (mouseX == 0)
				camera->RotateY(-previousFrame*2.0f);

			if (mouseX == graphics->GetWindow()->getWidth()-1.0f)
				camera->RotateY(previousFrame*2.0f);

			if (mouseY == 0)
				camera->Pitch(-previousFrame*2.0f);

			if (mouseY == graphics->GetWindow()->getHeight()-1.0f)
				camera->Pitch(previousFrame*2.0f);

			lastMousePosX = mouseX;
			lastMousePosY = mouseY;

			auto rect = graphics->GetWindow()->getWindowRect();
			ClipCursor(rect);

			camera->Update();

			//update splines
			
			XMFLOAT3 splineProgress = (pow((1 - progress), 3)*splines[splineIndex]->StartPoint) +
				((3 * progress*pow((1 - progress), 2))*splines[splineIndex]->ControlPoint1) +
				(3 * pow(progress, 2)*(1 - progress)*splines[splineIndex]->ControlPoint2) +
				(pow(progress, 3)*splines[splineIndex]->EndPoint);

			objects[5]->Update(splineProgress);

			progress += previousFrame * 0.5f;

			if (progress > 1.0f)
			{
				progress = 0.0f;
				if (splineIndex != 3)
					splineIndex++;
				else
					splineIndex = 0;
			}

			for each (Object* object in objects)
			{
				object->Render(); 
			}

			// Present the information rendered to the back buffer to the front buffer
			graphics->PresentSwapChain();

			unsigned int end = timeGetTime();
			previousFrame = (end - start) / 1000.0f;
		}
	}
	return msg;
}

void Game::CleanUp()
{
	for each (Object* object in objects)
	{
		object->CleanUp();
	}
}