#ifndef CONSTANTBUFFER_H
#define CONSTANTBUFFER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <vector>
#include "Structures.h"

class ConstantBuffer
{
private:
	ID3D11Buffer* g_pConstantBuffer = NULL;
	
public:
	template <typename T> HRESULT CreateConstantBuffer(ID3D11Device* g_pd3dDevice)
	{
		D3D11_BUFFER_DESC cbDesc;
		ZeroMemory(&cbDesc, sizeof(cbDesc));
		cbDesc.ByteWidth = sizeof(T);
		cbDesc.Usage = D3D11_USAGE_DEFAULT;
		cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		cbDesc.CPUAccessFlags = 0;
		cbDesc.MiscFlags = 0;
		cbDesc.StructureByteStride = 0;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&cbDesc, NULL, &g_pConstantBuffer);
		if (FAILED(hr))
			return hr;
	}
	void Release();

	ID3D11Buffer* GetBuffer();
};

#endif