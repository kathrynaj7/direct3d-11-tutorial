#include "IndexBuffer.h"

HRESULT IndexBuffer::CreateIndexBuffer(ID3D11Device* g_pd3dDevice, std::vector<unsigned int> indices)
{
	D3D11_BUFFER_DESC ibd;
	ZeroMemory(&ibd, sizeof(ibd));
	ibd.Usage = D3D11_USAGE_DEFAULT;
	ibd.ByteWidth = sizeof(unsigned int) * indices.size();
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = indices.data();
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	HRESULT hr = g_pd3dDevice->CreateBuffer(&ibd, &InitData, &g_pIndexBuffer);
	if (FAILED(hr))
		return hr;
};

void IndexBuffer::SetActiveBuffer(ID3D11DeviceContext* g_pImmediateContext)
{
	g_pImmediateContext->IASetIndexBuffer(g_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
}

void IndexBuffer::Release()
{
	if (g_pIndexBuffer) g_pIndexBuffer->Release();
}