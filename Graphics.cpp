#include "Graphics.h"
#include "Window.h"
#include "ConstantBuffer.h"
#include "Structures.h"

using namespace DirectX;

Graphics::Graphics(Window* window)
{
	this->window = window;
}

Graphics::~Graphics(void)
{
}

//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT Graphics::InitDevice(HWND g_hWnd)
{
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// Create the device and swap chain
	hr = CreateDeviceAndSwapCahin(width, height, g_hWnd);
	// Create back buffer and render target view
	hr = CreateRenderTargetView();
	// Create Depth Stencil texture
	hr = CreateDepthStencilTexture(width, height);	
	// Create Depth Stencil state
	hr = CreateDepthStencilState();
	// Bind Depth Stencil state
	g_pImmediateContext->OMSetDepthStencilState(g_pDSState, 1);
	// Create Depth Stencil view
	hr = CreateDepthStencilView();		
	// Bind Depth Stencil view
	g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, g_pDepthStencilView);
	// Setup the viewport
	SetViewport(width, height);
	// Create rasterizer state
	hr = CreateRasterizerState();
	// Set rasterizer state
	g_pImmediateContext->RSSetState(g_pRasterizerState);
	// Create linear sampler state
	hr = CreateLinearSampler();
	// Set linear sampler
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);

	// Create World constant buffer
	ConstantBuffer*  worldConstBuffer = new ConstantBuffer();
	worldConstBuffer->CreateConstantBuffer<WorldMatrix>(g_pd3dDevice);
	worldBuffer = worldConstBuffer->GetBuffer();

	// Create Projection constant buffer
	projectionConstBuffer = new ConstantBuffer();
	projectionConstBuffer->CreateConstantBuffer<ProjectionMatrix>(g_pd3dDevice);
	ID3D11Buffer* projectionBuffer = projectionConstBuffer->GetBuffer();
	ProjectionMatrix projectionMatrix;
	projectionMatrix.Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, (float)width / (float)height, 0.1f, 1000.0f);
	g_pImmediateContext->UpdateSubresource(projectionBuffer, 0, NULL, &projectionMatrix, 0, 0);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &projectionBuffer);

	// Set primitive topology
	g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create the device and swap chain
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateDeviceAndSwapCahin(UINT width, UINT height, HWND g_hWnd)
{
	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	// Swap chain description
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = g_hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	// Create swap chain
	HRESULT hr;
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		g_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &g_featureLevel, &g_pImmediateContext);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Create back buffer and render target view
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateRenderTargetView()
{
	ID3D11Texture2D* pBackBuffer = NULL;
	HRESULT hr = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;

	hr = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_pRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Create the Depth Stencil texture
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateDepthStencilTexture(UINT width, UINT height)
{
	D3D11_TEXTURE2D_DESC dSD;
	ZeroMemory(&dSD, sizeof(dSD));
	dSD.Width = width;
	dSD.Height = height;
	dSD.MipLevels = 1;
	dSD.ArraySize = 1;
	dSD.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dSD.SampleDesc.Count = 1;
	dSD.SampleDesc.Quality = 0;
	dSD.Usage = D3D11_USAGE_DEFAULT;
	dSD.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	dSD.CPUAccessFlags = 0;
	dSD.MiscFlags = 0;

	HRESULT hr = g_pd3dDevice->CreateTexture2D(&dSD, NULL, &g_pDepthStencil);
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Create the Depth Stencil state
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateDepthStencilState()
{
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	ZeroMemory(&dsDesc, sizeof(dsDesc));
	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	HRESULT hr = g_pd3dDevice->CreateDepthStencilState(&dsDesc, &g_pDSState);
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Create the Depth Stencil view
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateDepthStencilView()
{
	D3D11_DEPTH_STENCIL_VIEW_DESC dSVD;
	ZeroMemory(&dSVD, sizeof(dSVD));
	dSVD.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dSVD.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dSVD.Texture2D.MipSlice = 0;

	// Create the depth stencil view
	HRESULT hr = g_pd3dDevice->CreateDepthStencilView(g_pDepthStencil, &dSVD, &g_pDepthStencilView);
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Setup the viewport
//--------------------------------------------------------------------------------------
void Graphics::SetViewport(UINT width, UINT height)
{
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pImmediateContext->RSSetViewports(1, &vp);
}

//--------------------------------------------------------------------------------------
// Create a rasterizer state
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateRasterizerState()
{
	ZeroMemory(&g_prsDesc, sizeof(D3D11_RASTERIZER_DESC));
	g_prsDesc.FillMode = D3D11_FILL_SOLID; // Use FILL_WIREFRAME for wireframe rendering. FILL_SOLID for normal.
	g_prsDesc.CullMode = D3D11_CULL_BACK; // Use CULL_BACK for back-face culling, CULL_NONE for no culling, CULL_FRONT for front-face culling.
	g_prsDesc.FrontCounterClockwise = false;
	g_prsDesc.DepthClipEnable = true;
	g_prsDesc.AntialiasedLineEnable = false;

	HRESULT hr = g_pd3dDevice->CreateRasterizerState(&g_prsDesc, &g_pRasterizerState);
	if (FAILED(hr))
		return hr;
}

HRESULT Graphics::UpdateRasterizer(D3D11_RASTERIZER_DESC desc)
{
	g_prsDesc = desc;
	HRESULT hr = g_pd3dDevice->CreateRasterizerState(&desc, &g_pRasterizerState);
	if (FAILED(hr))
		return hr;
	g_pImmediateContext->RSSetState(g_pRasterizerState);
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create a linear sampler state
//--------------------------------------------------------------------------------------
HRESULT Graphics::CreateLinearSampler()
{
	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRESULT hr = g_pd3dDevice->CreateSamplerState(&sampDesc, &g_pSamplerLinear);
	if (FAILED(hr))
		return hr;
}

//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void Graphics::CleanupDevice()
{
	if (g_pImmediateContext) g_pImmediateContext->ClearState();

	projectionConstBuffer->Release();
	if (g_pDepthStencilView) g_pDepthStencilView->Release();
	if (g_pDSState) g_pDSState->Release();
	if (g_pDepthStencil) g_pDepthStencil->Release();
	if (g_pRenderTargetView) g_pRenderTargetView->Release();
	if (g_pSwapChain) g_pSwapChain->Release();
	if (g_pImmediateContext) g_pImmediateContext->Release();
	if (g_pd3dDevice) g_pd3dDevice->Release();
}

//--------------------------------------------------------------------------------------
// Clear the back buffer
//--------------------------------------------------------------------------------------
void Graphics::ClearBackBuffer()
{
	float ClearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f }; // red,green,blue,alpha
	g_pImmediateContext->ClearRenderTargetView(g_pRenderTargetView, ClearColor);
}

//--------------------------------------------------------------------------------------
// Clear the depth stencil
//--------------------------------------------------------------------------------------
void Graphics::ClearDepthStencil()
{
	g_pImmediateContext->ClearDepthStencilView(g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

//--------------------------------------------------------------------------------------
// Present the swap chain
//--------------------------------------------------------------------------------------
void Graphics::PresentSwapChain()
{
	g_pSwapChain->Present(0, 0);
}

//--------------------------------------------------------------------------------------
// Get Device
//--------------------------------------------------------------------------------------
ID3D11Device* Graphics::GetDevice()
{
	return g_pd3dDevice;
}

//--------------------------------------------------------------------------------------
// Get context
//--------------------------------------------------------------------------------------
ID3D11DeviceContext* Graphics::GetImmediateContext()
{
	return g_pImmediateContext;
}

//--------------------------------------------------------------------------------------
// Get world buffer
//--------------------------------------------------------------------------------------
ID3D11Buffer* Graphics::GetWorldBuffer()
{
	return worldBuffer;
}

//--------------------------------------------------------------------------------------
// Get window
//--------------------------------------------------------------------------------------
Window* Graphics::GetWindow()
{
	return window;
}

//--------------------------------------------------------------------------------------
// Get rasterizer description
//--------------------------------------------------------------------------------------
D3D11_RASTERIZER_DESC Graphics::GetRasterizerDesc()
{
	return g_prsDesc;
}

//--------------------------------------------------------------------------------------
// Get rasterizer state
//--------------------------------------------------------------------------------------
ID3D11RasterizerState* Graphics::GetRasterizerState()
{
	return g_pRasterizerState;
}