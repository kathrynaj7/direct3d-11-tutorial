//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
cbuffer WorldConstBuffer : register(b0)
{
	matrix World;
};

cbuffer ViewConstBuffer : register(b1)
{
	matrix View;
};

cbuffer ProjectionConstBuffer : register(b2)
{
	matrix Projection;
};

cbuffer CameraDataConstBuffer : register(b3)
{
	float3 CameraPos;
};

TextureCube cubeMap : register(t0);

SamplerState samSky : register(s0)
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VS_INPUT
{
	float4 Pos : POSITION;
	float4 Norm : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 Norm : TEXCOORD0;
	float3 Tex : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	output.Pos = mul(Projection, mul(View, mul(World, input.Pos))).xyzw;
	output.Norm = input.Norm;
	output.Tex = input.Pos.xyz;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float3 texColour = cubeMap.Sample(samSky, input.Tex);
	return float4(texColour.rgb, 1.0f);
}