//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
cbuffer WorldConstBuffer : register(b0)
{
	matrix World;
};

cbuffer ViewConstBuffer : register(b1)
{
	matrix View;
};

cbuffer ProjectionConstBuffer : register(b2)
{
	matrix Projection;
};

cbuffer CameraDataConstBuffer : register(b3)
{
	float3 CameraPos;
};

Texture2D txDiffuse : register(t0);

SamplerState samLinear : register(s0);

struct VS_INPUT
{
	float4 Pos : POSITION;
	float4 Norm : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PATCH
{
	float TessFactor[3] : SV_TessFactor;
	float InsideTess : SV_InsideTessFactor;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 World : POSITION;
	float4 Norm : TEXCOORD0;
	float2 Tex : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
	output.World = mul(World, input.Pos);
	output.Norm = normalize(mul(World, input.Norm.xyz));
	output.Tex = input.Tex;

	return output;
}

//--------------------------------------------------------------------------------------
// Constant Hull Shader
//--------------------------------------------------------------------------------------
PATCH ConstantHS(InputPatch<PS_INPUT, 3> patch, uint PatchID : SV_PrimitiveID)
{
	PATCH output;

	output.TessFactor[0] = 3;
	output.TessFactor[1] = 3;
	output.TessFactor[2] = 3;

	output.InsideTess = 3;

	return output;
}

//--------------------------------------------------------------------------------------
// Control Point Hull Shader
//--------------------------------------------------------------------------------------
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ConstantHS")]
PS_INPUT HS(InputPatch<PS_INPUT, 3> patch, uint i : SV_OutputControlPointID, uint PatchID : SV_PrimitiveID)
{
	PS_INPUT output;

	output.Pos = patch[i].Pos;
	output.World = patch[i].World;
	output.Norm = patch[i].Norm;
	output.Tex = patch[i].Tex;

	return output;
}

//--------------------------------------------------------------------------------------
// Domain Shader
//--------------------------------------------------------------------------------------
[domain("tri")]
PS_INPUT DS(PATCH patch, float3 uv : SV_DomainLocation, const OutputPatch<PS_INPUT, 3> tri)
{
	PS_INPUT output;

	output.Pos = tri[0].Pos * uv.x + tri[1].Pos * uv.y + tri[2].Pos * uv.z;
	output.World = tri[0].World * uv.x + tri[1].World * uv.y + tri[2].World * uv.z;
	output.Norm = tri[0].Norm * uv.x + tri[1].Norm * uv.y + tri[2].Norm * uv.z;
	output.Tex = tri[0].Tex * uv.x + tri[1].Tex * uv.y + tri[2].Tex * uv.z;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float3 lightDir = normalize(input.World.xyz - float3(0.0f, 3.0f, 0.0f));
	float4 lightCol = { 1.0f, 1.0f, 1.0f, 1.0f };
	float4 textureColour = txDiffuse.Sample(samLinear, input.Tex);

	float4 ambient = { 0.0f, 0.0f, 0.0f, 1.0f };

	float diffuse = saturate(max(dot(input.Norm.xyz, -lightDir), 0));

	float3 viewVector = normalize(CameraPos.xyz - input.World.xyz);
	float3 reflection = normalize(reflect(lightDir, input.Norm).xyz);
	float specular = pow(max(dot(viewVector, reflection), 0), 20);

	return textureColour * (ambient + (diffuse * lightCol) + (specular * lightCol));
}