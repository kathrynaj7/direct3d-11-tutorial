#ifndef VERTEXSHADER_H
#define VERTEXSHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Shader.h"

class Graphics;

class VertexShader : Shader
{
public:
	virtual HRESULT InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext);
	virtual void SetShader(ID3D11DeviceContext* g_pImmediateContext);
	virtual void CleanUp();

private:
	ID3DBlob*				pVSBlob = NULL;
	ID3D11VertexShader*		g_pVertexShader = NULL;
	ID3D11InputLayout*		g_pVertexLayout = NULL;
};

#endif