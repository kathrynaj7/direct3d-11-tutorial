#ifndef OBJECT_H
#define OBJECT_H

#include <vector>
#include <d3d11.h>
#include <d3dcompiler.h>
#include "Structures.h"

class Graphics;
class VertexBuffer;
class IndexBuffer;
class VertexShader;
class PixelShader;
class HullShader;
class DomainShader;

class Object
{
private:
	Graphics*		graphics;

	VertexBuffer*	vertexBuffer;
	IndexBuffer*	indexBuffer;

	VertexShader*	vertexShader;
	PixelShader*	pixelShader;
	HullShader*		hullShader;
	DomainShader*	domainShader;

	std::wstring shaderName;

	ID3D11ShaderResourceView* modelTexture;
	ID3D11ShaderResourceView* specularMap;
	ID3D11ShaderResourceView* normalMap;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	DirectX::XMFLOAT3 translation;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMFLOAT3 scale;

public:
	Object(Graphics* graphics);
	~Object(void);

	HRESULT InitObject(std::string OBJfilename, std::wstring DDSfilename, std::wstring TextureFilename, std::wstring SpecularFilename, std::wstring NormalFilename, std::wstring PSfilename, std::wstring HSfilename, std::wstring DSfilename, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, DirectX::XMFLOAT3 scale);
	void Render();
	void Update(DirectX::XMFLOAT3 position);
	void CleanUp();
};

#endif