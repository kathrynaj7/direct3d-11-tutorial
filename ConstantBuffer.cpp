#include "ConstantBuffer.h"

void ConstantBuffer::Release()
{
	g_pConstantBuffer->Release();
}

ID3D11Buffer* ConstantBuffer::GetBuffer()
{
	return g_pConstantBuffer;
}