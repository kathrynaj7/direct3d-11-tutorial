//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
cbuffer WorldConstBuffer : register(b0)
{
	matrix World;
};

cbuffer ViewConstBuffer : register(b1)
{
	matrix View;
};

cbuffer ProjectionConstBuffer : register(b2)
{
	matrix Projection;
};

cbuffer CameraDataConstBuffer : register(b3)
{
	float3 CameraPos;
};

Texture2D txDiffuse : register(t0);
Texture2D txSpecular : register(t1);
Texture2D txNormal : register(t2);

SamplerState samLinear : register(s0);

struct VS_INPUT
{
	float4 Pos : POSITION;
	float4 Norm : NORMAL;
	float2 Tex : TEXCOORD;
	float4 Tan : TANGENT;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 World : POSITION;
	float4 Norm : TEXCOORD0;
	float2 Tex : TEXCOORD1;
	float4 Tan : TEXCOORD2;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
	output.World = mul(World, input.Pos);
	output.Norm = normalize(mul(World, input.Norm.xyz));
	output.Tex = input.Tex;
	output.Tan = normalize(mul(World, input.Tan.xyz));

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float3 lightDir = normalize(input.World.xyz - float3(0.0f, 3.0f, 0.0f));
	float4 lightCol = { 1.0f, 1.0f, 1.0f, 1.0f };
	float4 textureColour = txDiffuse.Sample(samLinear, input.Tex);

	float4 ambient = { 0.0f, 0.0f, 0.0f, 1.0f };

	float3 bitangent = normalize(cross(input.Tan.xyz, input.Norm.xyz));
	float3 bumpNorm = 2.0 * (txNormal.Sample(samLinear, input.Tex).xyz) - float3(1.0f, 1.0f, 1.0f);
	float3x3 TanBiNorm = float3x3(input.Tan.xyz, bitangent, input.Norm.xyz);
	input.Norm.xyz = normalize(mul(bumpNorm, TanBiNorm));

	float diffuse = saturate(max(dot(input.Norm.xyz, -lightDir), 0));
	
	float3 viewVector = normalize(CameraPos.xyz - input.World.xyz);
	float3 reflection = normalize(reflect(lightDir, input.Norm).xyz);
	float specular = pow(max(dot(viewVector, reflection), 0), 20);

	return textureColour * (ambient + (diffuse * lightCol) + (specular * lightCol * txSpecular.Sample(samLinear, input.Tex)));
}