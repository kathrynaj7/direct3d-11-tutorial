//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
cbuffer WorldConstBuffer : register(b0)
{
	matrix World;
};

cbuffer ViewConstBuffer : register(b1)
{
	matrix View;
};

cbuffer ProjectionConstBuffer : register(b2)
{
	matrix Projection;
};

cbuffer CameraDataConstBuffer : register(b3)
{
	float3 CameraPos;
};

TextureCube cubeMap : register(t0);

SamplerState samEnv : register(s0)
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VS_INPUT
{
	float4 Pos : POSITION;
	float4 Norm : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 World : POSITION;
	float4 Norm : TEXCOORD0;
	float2 Tex : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	output.Pos = mul(Projection, mul(View, mul(World, input.Pos)));
	output.World = mul(World, input.Pos);
	output.Norm = normalize(mul(World, input.Norm.xyz));
	output.Tex = input.Tex;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float3 lightDir = normalize(input.World.xyz - float3(0.0f, 3.0f, 0.0f));
	float4 lightCol = { 1.0f, 1.0f, 1.0f, 1.0f };

	float4 ambient = { 0.0f, 0.0f, 0.0f, 1.0f };

	float diffuse = saturate(max(dot(input.Norm.xyz, -lightDir), 0));

	float3 viewVector = normalize(CameraPos.xyz - input.World.xyz);
	float3 reflection = normalize(reflect(lightDir, input.Norm).xyz);
	float specular = pow(max(dot(viewVector, reflection), 0), 20);

	float3 eyeReflection = reflect(-viewVector, input.Norm.xyz);

	return (ambient + (diffuse * lightCol) + (specular * lightCol)) * cubeMap.Sample(samEnv, eyeReflection);
}