#ifndef DOMAINSHADER_H
#define DOMAINSHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Shader.h"

class Graphics;

class DomainShader : Shader
{
public:
	virtual HRESULT InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext);
	virtual void SetShader(ID3D11DeviceContext* g_pImmediateContext);
	virtual void CleanUp();

private:
	ID3DBlob*				pDSBlob = NULL;
	ID3D11DomainShader*		g_pDomainShader = NULL;
};

#endif