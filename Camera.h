#ifndef CAMERA_H
#define CAMERA_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include "Structures.h"

class Graphics;

class Camera
{
private:
	Graphics* graphics;
	ID3D11Buffer* viewBuffer;
	ID3D11Buffer* cameraDataBuffer;

	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 direction;
	DirectX::XMFLOAT3 up;
	DirectX::XMFLOAT3 right;

	ViewMatrix view;

public:
	Camera(Graphics* graphics);
	~Camera(void);

	HRESULT InitCamera(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 direction, DirectX::XMFLOAT3 up, DirectX::XMFLOAT3 right);
	void Update();

	void Cleanup();

	ViewMatrix getView();

	void Strafe(float deltaTime);
	void Walk(float deltaTime);

	void Pitch(float angle);
	void RotateY(float angle);
};

#endif