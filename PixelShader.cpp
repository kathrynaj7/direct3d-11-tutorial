#include "PixelShader.h"
#include "Graphics.h"

HRESULT PixelShader::InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext)
{
	HRESULT hr = CompileShaderFromFile(name.c_str(), "PS", "ps_4_0", &pPSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pPixelShader);
	pPSBlob->Release();
	if (FAILED(hr))
		return hr;
}

void PixelShader::SetShader(ID3D11DeviceContext* g_pImmediateContext)
{
	g_pImmediateContext->PSSetShader(g_pPixelShader, NULL, 0);
}

void PixelShader::CleanUp()
{
	g_pPixelShader->Release();
}