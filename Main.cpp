#include <Windows.h>
#include "Window.h"
#include "Graphics.h"
#include "Game.h"

Window* window;
Graphics* graphics;
Game* game;

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	window = new Window(hInstance);
	if (FAILED(window->InitWindow(hInstance, nCmdShow)))
		return 0;

	graphics = new Graphics(window);
	if (FAILED(graphics->InitDevice(window->getHWnd())))
	{
		graphics->CleanupDevice();
		return 0;
	}

	game = new Game(graphics);
	if (FAILED(game->InitGame()))
		return 0;

	MSG msg = game->Run(window->getHWnd());

	game->CleanUp();
	graphics->CleanupDevice();

	return (int)msg.wParam;
}
