#include "Object.h"
#include "Graphics.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "PixelShader.h"
#include "ModelLoader.h"
#include "TextureLoader.h"

using namespace DirectX;

Object::Object(Graphics* graphics)
{
	this->graphics = graphics;
}

Object::~Object(void)
{
}

HRESULT Object::InitObject(std::string OBJfilename, std::wstring TextureFilename, std::wstring SpecularFilename, std::wstring NormalFilename, std::wstring VSfilename, std::wstring PSfilename, std::wstring HSfilename, std::wstring DSfilename, XMFLOAT3 translation, XMFLOAT3 rotation, XMFLOAT3 scale)
{
	shaderName = HSfilename;

	// Load in models
	ModelLoader::LoadModel(OBJfilename, vertices, indices);

	HRESULT hr = CreateDDSTextureFromFile(graphics->GetDevice(), TextureFilename.c_str(), nullptr, &modelTexture);
	if (FAILED(hr))
		return hr;

	hr = CreateDDSTextureFromFile(graphics->GetDevice(), SpecularFilename.c_str(), nullptr, &specularMap);
	if (FAILED(hr))
		return hr;

	hr = CreateDDSTextureFromFile(graphics->GetDevice(), NormalFilename.c_str(), nullptr, &normalMap);
	if (FAILED(hr))
		return hr;

	// Setup vertex shader
	vertexShader = new VertexShader();
	vertexShader->InitShader(VSfilename.c_str(), graphics->GetDevice(), graphics->GetImmediateContext());

	if (shaderName == L"Tesselate.fx")
	{
		hullShader = new HullShader();
		hullShader->InitShader(HSfilename.c_str(), graphics->GetDevice(), graphics->GetImmediateContext());
		domainShader = new DomainShader();
		domainShader->InitShader(DSfilename.c_str(), graphics->GetDevice(), graphics->GetImmediateContext());
	}

	// Setup pixel shader
	pixelShader = new PixelShader();
	pixelShader->InitShader(PSfilename.c_str(), graphics->GetDevice(), graphics->GetImmediateContext());

	vertexBuffer = new VertexBuffer();
	vertexBuffer->CreateVertexBuffer(graphics->GetDevice(), vertices);

	indexBuffer = new IndexBuffer();
	indexBuffer->CreateIndexBuffer(graphics->GetDevice(), indices);

	this->translation = translation;
	this->rotation = rotation;
	this->scale = scale;
}

void Object::Render()
{
		vertexShader->SetShader(graphics->GetImmediateContext());
	if (shaderName == L"Tesselate.fx")
	{
		hullShader->SetShader(graphics->GetImmediateContext());
		domainShader->SetShader(graphics->GetImmediateContext());
		graphics->GetImmediateContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
	}
	else
	{
		graphics->GetImmediateContext()->HSSetShader(NULL, NULL, 0);
		graphics->GetImmediateContext()->DSSetShader(NULL, NULL, 0);
		graphics->GetImmediateContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}
	pixelShader->SetShader(graphics->GetImmediateContext());

	graphics->GetImmediateContext()->PSSetShaderResources(0, 1, &modelTexture);
	graphics->GetImmediateContext()->PSSetShaderResources(1, 1, &specularMap);
	graphics->GetImmediateContext()->PSSetShaderResources(2, 1, &normalMap);

	vertexBuffer->SetActiveBuffer(graphics->GetImmediateContext());
	indexBuffer->SetActiveBuffer(graphics->GetImmediateContext());

	WorldMatrix worldMatrix;
	worldMatrix.World = XMMatrixScaling(scale.x, scale.y, scale.z)*
						XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z)*
						XMMatrixTranslation(translation.x, translation.y, translation.z);

	ID3D11Buffer* worldBuffer = graphics->GetWorldBuffer();
	graphics->GetImmediateContext()->UpdateSubresource(worldBuffer, 0, NULL, &worldMatrix, 0, 0);
	graphics->GetImmediateContext()->VSSetConstantBuffers(0, 1, &worldBuffer);

	graphics->GetImmediateContext()->DrawIndexed(indices.size(), 0, 0);
}

void Object::Update(DirectX::XMFLOAT3 position)
{
	this->translation = position;
}

void Object::CleanUp()
{
	vertexBuffer->Release();
	indexBuffer->Release();

	vertexShader->CleanUp();
	pixelShader->CleanUp();
}