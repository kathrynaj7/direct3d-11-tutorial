#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <DirectXMath.h>

//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
struct Vertex
{
	Vertex(DirectX::XMFLOAT3 Pos, DirectX::XMFLOAT3 Normal, DirectX::XMFLOAT2 TextCoord)
	{
		this->Pos = Pos;
		this->Normal = Normal;
		this->TextCoord = TextCoord;
	}
	DirectX::XMFLOAT3 Pos;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 TextCoord;
	DirectX::XMFLOAT3 Tangent;
};

struct ProjectionMatrix
{
	DirectX::XMMATRIX Projection;
};

struct WorldMatrix
{
	DirectX::XMMATRIX World;
};

struct ViewMatrix
{
	DirectX::XMMATRIX View;
};

struct CameraData
{
	DirectX::XMFLOAT3 CameraPos;
	int pad;
};

struct BezierSpline
{
	DirectX::XMFLOAT3 StartPoint;
	DirectX::XMFLOAT3 ControlPoint1;
	DirectX::XMFLOAT3 ControlPoint2;
	DirectX::XMFLOAT3 EndPoint;

	BezierSpline(DirectX::XMFLOAT3 StartPoint, DirectX::XMFLOAT3 ControlPoint1, DirectX::XMFLOAT3 ControlPoint2, DirectX::XMFLOAT3 EndPoint)
	{
		this->StartPoint = StartPoint;
		this->ControlPoint1 = ControlPoint1;
		this->ControlPoint2 = ControlPoint2;
		this->EndPoint = EndPoint;
	}
};

DirectX::XMFLOAT3 operator*(float f, DirectX::XMFLOAT3 xf);
DirectX::XMFLOAT3 operator*(DirectX::XMFLOAT3 xf, float f);
DirectX::XMFLOAT3 operator+(DirectX::XMFLOAT3 xf1, DirectX::XMFLOAT3 xf2);

#endif