#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <vector>
#include "Structures.h"

class VertexBuffer
{
private:
	ID3D11Buffer* g_pVertexBuffer = NULL;

public:
	HRESULT CreateVertexBuffer(ID3D11Device* g_pd3dDevice, std::vector<Vertex> vertices);
	void SetActiveBuffer(ID3D11DeviceContext* g_pImmediateContext);
	void Release();
};

#endif