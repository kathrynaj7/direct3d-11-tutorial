#include "DomainShader.h"
#include "Graphics.h"

HRESULT DomainShader::InitShader(std::wstring name, ID3D11Device* g_pd3dDevice, ID3D11DeviceContext* g_pImmediateContext)
{
	HRESULT hr = CompileShaderFromFile(name.c_str(), "DS", "ds_5_0", &pDSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = g_pd3dDevice->CreateDomainShader(pDSBlob->GetBufferPointer(), pDSBlob->GetBufferSize(), NULL, &g_pDomainShader);
	pDSBlob->Release();
	if (FAILED(hr))
		return hr;
}

void DomainShader::SetShader(ID3D11DeviceContext* g_pImmediateContext)
{
	g_pImmediateContext->DSSetShader(g_pDomainShader, NULL, 0);
}

void DomainShader::CleanUp()
{
	g_pDomainShader->Release();
}