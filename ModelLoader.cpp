#include <string>
#include <iostream>
#include <fstream>
#include "ModelLoader.h"

using namespace std;
using namespace DirectX;

bool ModelLoader::LoadModel(string name, vector<Vertex>& vertices, vector<unsigned int>& indices)
{
	vector<XMFLOAT3> fileVertices;
	vector<XMFLOAT3> fileNormals;
	vector<XMFLOAT2> fileTextures;

	int vertexCount = 0;

	int temp = 0;
	ifstream file;
	string line;
	file.open(name, ios::in);
	if (!file.good()) return false;

	while (getline(file, line))
	{
		if (line.size() > 0)
		{
			if (line[0] == '#') {}

			else if (line[0] == 'v' && line[1] == ' ')
			{
				fileVertices.push_back(ProcessLineToFloat3(line));
			}
			else if (line[0] == 'v' && line[1] == 'n')
			{
				fileNormals.push_back(ProcessLineToFloat3(line));
			}
			else if (line[0] == 'v' && line[1] == 't')
			{
				fileTextures.push_back(ProcessLineToFloat2(line));
			}
			else if (line[0] == 'f' && line[1] == ' ')
			{
				ProcessFace(vertices, line, fileVertices, fileNormals, fileTextures, vertexCount);
			}
			else
			{
				temp = 0;
			}
		}
	}

	for (int i = 0; i < vertexCount; i++)
	{
		indices.push_back(i);
	}

	CalculateTangents(vertices, indices);

	return true;
}

XMFLOAT2 ModelLoader::ProcessLineToFloat2(string line)
{
	XMFLOAT2 returnValue = XMFLOAT2();

	char* token = strtok((char*)line.c_str(), " ");

	returnValue.x = atof(strtok(NULL, " "));
	returnValue.y = atof(strtok(NULL, " "));

	return returnValue;
}

XMFLOAT3 ModelLoader::ProcessLineToFloat3(string line)
{
	XMFLOAT3 returnValue = XMFLOAT3();

	char* token = strtok((char*)line.c_str(), " ");

	returnValue.x = atof(strtok(NULL, " "));
	returnValue.y = atof(strtok(NULL, " "));
	returnValue.z = atof(strtok(NULL, " "));

	return returnValue;
}

void ModelLoader::ProcessFace(vector<Vertex>& vertices, string line, 
							  vector<XMFLOAT3>fileVertices, 
							  vector<XMFLOAT3>fileNormals, 
							  vector<XMFLOAT2>fileTextures, 
							  int &vertexCount)
{
	XMFLOAT3 triangle = XMFLOAT3();

	char* token = strtok((char*)line.c_str(), " /");
	for (int i = 0; i < 3; i++)
	{
		triangle.x = atof(strtok(NULL, " /"));
		triangle.y = atof(strtok(NULL, " /"));
		triangle.z = atof(strtok(NULL, " /"));

		vertices.push_back(Vertex(XMFLOAT3(fileVertices[triangle.x-1]), 
								  XMFLOAT3(fileNormals[triangle.z-1]), 
								  XMFLOAT2(fileTextures[triangle.y-1])));
		vertexCount++;
	}
}

void ModelLoader::CalculateTangents(vector<Vertex>& vertices, vector<unsigned int>& indices)
{
	vector<XMFLOAT3> tan1 = vector<XMFLOAT3>();

	for (unsigned int i = 0; i < indices.size(); i += 3)
	{
		int i1 = indices[i];
		int i2 = indices[i + 1];
		int i3 = indices[i + 2];

		XMFLOAT3 v1 = vertices[i1].Pos;
		XMFLOAT3 v2 = vertices[i2].Pos;
		XMFLOAT3 v3 = vertices[i3].Pos;

		XMFLOAT2 w1 = vertices[i1].TextCoord;
		XMFLOAT2 w2 = vertices[i2].TextCoord;
		XMFLOAT2 w3 = vertices[i3].TextCoord;

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0f / (s1 * t2 - s2 * t1);

		XMFLOAT3 sdir = XMFLOAT3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);

		for (int i = 0; i < 3; i++)
			tan1.push_back(sdir);
	}

	for (unsigned int i = 0; i < vertices.size(); ++i)
	{
		XMFLOAT3 n = vertices[i].Normal;
		XMFLOAT3 t = tan1[i];

		XMFLOAT3 tmp = XMSubtract(t, (XMMultiplyByFloat(n, DotProduct(n, t))));

		vertices[i].Tangent = XMNormalise(tmp);
	}
}

float ModelLoader::DotProduct(XMFLOAT3 one, XMFLOAT3 two)
{
	return ((one.x * two.x) + (one.y * two.y) + (one.z * two.z));
}

XMFLOAT3 ModelLoader::XMMultiplyByFloat(XMFLOAT3 one, float two)
{
	return XMFLOAT3(one.x * two, one.y * two, one.z * two);
}

XMFLOAT3 ModelLoader::XMSubtract(XMFLOAT3 one, XMFLOAT3 two)
{
	return XMFLOAT3(one.x - two.x, one.y - two.y, one.z - two.z);
}

XMFLOAT3 ModelLoader::XMNormalise(XMFLOAT3 one)
{
	float magnitude = sqrt((one.x * one.x) + (one.y * one.y) + (one.z * one.z));
	return XMFLOAT3(one.x / magnitude, one.y / magnitude, one.z / magnitude);
}
