#ifndef WINDOW_H
#define WINDOW_H

#include "Resource.h"
#include <windows.h>

class Window
{
private:
	HINSTANCE g_hInst = NULL;
	HWND g_hWnd = NULL;
	static float mousePosX;
	static float mousePosY;
	RECT* sizeOfWindow = NULL;

public:
	Window(HINSTANCE hInstance);
	~Window(void);

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	HINSTANCE getHInst();
	HWND getHWnd();
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	int getWidth();
	int getHeight();

	float getMousePosX();
	float getMousePosY();

	RECT* getWindowRect();
};

#endif